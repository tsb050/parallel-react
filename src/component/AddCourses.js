import React from 'react';

export default class AddOption extends React.Component{
    state ={
        error: undefined
    }
    
    submitOption = (e) => {
        e.preventDefault();
        const option = e.target.elements.option.value.trim();
        const error = this.props.addCourse(option)
        this.setState(() => ({ error }))

        if(!error){
            e.target.elements.option.value = "";
        }

    }
    render(){
        return(
            <div>
                {this.state.error && <p className="add_option_error">{this.state.error}</p>}
                <form onSubmit={this.submitOption} className="add-option">
                    <p>Enter a course </p>
                    <input className="add-text" type="text" name="option"></input>
                    <button className="button">Submit</button>
                </form>
            </div>
        )
    }
}