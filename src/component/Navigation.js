import React from 'react';
import { NavLink } from 'react-router-dom';

import '../style/App.css';
import homeIcon from '../images/homeImage.png';
import exitIcon from '../images/exitSign.jpg';


class Navigate extends React.Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <div className="navigation">
                <NavLink to="/welcome" className="normal" activeClassName="active" exact>
                    <div><img src={homeIcon} alt="Home Icon" height="20" width="30"/> Home</div>
                </NavLink>
                <NavLink to="/explore" className="normal" activeClassName="is-active"> Explore </NavLink>
                <NavLink to="/welcome" className="normal" activeClassName="is-active"> Videos</NavLink>
                <NavLink to="/welcome" className="normal" activeClassName="is-active"> Profile</NavLink>
                <NavLink to="/" className="normal" activeClassName="active" exact>
                    <div><img src={exitIcon} alt="Home Icon" height="20" width="30"/> Sign Out</div>
                </NavLink>
            </div> 
        )
    }
}

export default Navigate