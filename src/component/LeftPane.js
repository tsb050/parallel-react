import React from 'react';
import '../style/App.css';

const LeftPane = (props) => {
    return(
        <div className="profile">
            <div className="circular-profile">
                <img src={props.logo} alt="User image" height="200" width="170"/>  
            </div>
            <div className="divider"></div>
            <div className="profile-meta">
                <h4> Welcome {props.myName} </h4> 
                Some other stuff
            </div>
        </div>
    )
}

export default LeftPane;