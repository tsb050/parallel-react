import React from 'react';

import AddOption from './AddCourses';
import Navigate from './Navigation';
import ExploreMain from './ExploreMain';

import '../style/App.css';
import logo from '../images/profile.jpg';

class Explore extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            courses: [],
            picked: undefined
        }
    }

    handleAppendCourses = (value) => {
        if(!value){
            return 'Enter valid value'
        } else if(this.state.courses.indexOf(value) > -1) {
            return 'Already exists'
        }
        this.setState({courses: this.state.courses.concat(value)})
    }

    handleDeleteSingle = (val) => {
        this.setState((prev) => ({courses: prev.courses.filter((option) => {
            return val !== option;
        })}));
    }

    handleSelectValue = (val) => {
        this.setState({picked : val})
    }

    render(){
        return(
            <div className="explore">
                <div className="courseLeftPane">
                    <AddOption addCourse={this.handleAppendCourses} />
                    {
                        this.state.courses.map((course, index) => 
                            <div className="list_course"> 
                                <div>
                                    -
                                    <p>{course}</p>
                                    <button onClick={() => this.handleSelectValue(course)}>View</button>
                                    <button onClick={() => this.handleDeleteSingle(course)}>Remove</button>    
                                </div>
                               </div>)
                    }  
                </div>
                <ExploreMain className="courseMain" courseName={this.state.picked} />
                <Navigate />
            </div>
        )
    }
}

export default Explore