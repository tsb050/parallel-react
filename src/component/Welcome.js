import React from 'react';
// RCE CSS
import 'react-chat-elements/dist/main.css';
import { MessageList } from 'react-chat-elements'

import LeftPane from './LeftPane';
import Navigate from './Navigation';

import '../style/App.css';
import logo from '../images/profile.jpg';


class Welcome extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            messagePostion : 'right',
            messageType: 'text',
            messageText: undefined,
            messages: []
        }
    }

    componentDidMount() {
        try{
            //if(!this.props.isAuthed){
            //    this.props.history.push('/')
            //}else {
            const data = JSON.parse(localStorage.getItem('avl'))
            console.log(data)
            if(data.length != 0){
                this.setState({messages: data});
            }
            //}
        } catch(e){
            //Doing nothing
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(this.state.messages.length > 0 ){
            if(prevState.messages.length != this.state.messages.length){
                console.log(this.state.messages)
                const json = JSON.stringify(this.state.messages);
                localStorage.removeItem('avl');
                localStorage.setItem('avl', json);
            }
        }
    }

    handleTextInput = (e) => {
        this.setState({messageText: e.target.value});
    }

    onSubmit = (e) => {
        if(this.state.messageText != "" && this.state.messageText != undefined){
            const newMessage = {
                position : this.state.messagePostion,
                type: this.state.messageType,
                text: this.state.messageText,
                date: new Date
            }
            this.setState({ messages : [...this.state.messages, newMessage]})
        }
        this.setState({messageText: ""})
    }

    render(){
        return (
            <div className="mainContainer">
                <LeftPane myName={this.props.myName} logo={logo} />
                <div className="commune">
                    <div className="messenger">
                        <form>
                            <input type="text" placeholder="Enter message" onChange={this.handleTextInput}/>
                            <button type="reset" onClick={this.onSubmit}>Send</button>
                        </form>
                    </div>
                    <div>
                        <MessageList
                            className='message-list'
                            lockable={true}
                            toBottomHeight={'100%'}
                            dataSource={this.state.messages} />
                    </div>
                </div>
                <Navigate />
            </div>
        )
    }
};

export default Welcome;