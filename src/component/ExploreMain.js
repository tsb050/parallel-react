import React from 'react';
import { NavLink } from 'react-router-dom';

import '../style/App.css';

class ExploreMain extends React.Component{
    constructor(props){
        super(props);

        this.state = {
            courses: ['James', 'Monroe', 'Dwayne', 'Hart'],
            courseTime: "February",
            courseMaterials: {'IBM Networking':"https://www.ibm.com/cloud/learn/networking-a-complete-guide", 'Networking Fundamentals':"http://intronetworks.cs.luc.edu/current/ComputerNetworks.pdf"}
        }
    }

    render(){
        return(
            <div className={this.props.className}>
                {
                    this.props.courseName 
                    &&
                    <div className='real'>
                        <div className='col-1'>
                            <h3>{this.props.courseName}</h3>
                            <h4>{this.state.courseTime}</h4>
                        </div>
                        <div className='col-2'>
                            <h3>Useful Links</h3>
                            {Object.entries(this.state.courseMaterials).map(([key, value]) => <div><a href={value}>{key}</a></div>)}
                        </div>
                        <div className='col-3'>
                            <h3>Class Members</h3>
                            {this.state.courses.map(course => (<div>{course}</div>))}
                        </div>
                    </div>
                } 
            </div>
        )
    }
}
export default ExploreMain