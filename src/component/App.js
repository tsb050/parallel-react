import React from 'react';

import '../style/App.css';
import SignIn from './SignIn';
import SignUp from './SignUp';
import ImageDiv from './CoverImage';

class App extends React.Component {

  state = {
    show: true,
    count: 1
  }

  handleShowImage = () => {
    this.setState(() => ({ show: !this.state.show }))
    console.log(this.state.show);
  }

  handlePassName =(val) => {
    if(val){
      this.props.sendFromApp(val);
    }
  }

  render(){
    return (
      <div>
        <div>
          <h3>Hello, New here.</h3>
          {this.state.show ? <SignIn passName={this.handlePassName}/> : <ImageDiv />}
          {!this.state.show ? <SignUp /> : <ImageDiv />}
        </div>
        <div className="UpDiv" onClick={this.handleShowImage}>
            Sign up
        </div>
      </div>
      );
  }
};

export default App;

