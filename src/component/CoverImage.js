import React from 'react';
import '../style/App.css';
import logo from '../images/oceanView.jpg';

const ImageDiv = () => {
    return (
        <div className="Image_Div">
            <img src={logo} className="App-logo" alt="logo" />
        </div>
    );
}

export default ImageDiv;