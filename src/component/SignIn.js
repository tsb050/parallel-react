import React from 'react';
import '../style/App.css';
import { withRouter } from 'react-router-dom';

class SignIn extends React.Component {

    constructor(props){
      super(props)

      this.state = {
        display: 'block',
        error: false
      }
    }

    async getUsers(name, password) {
      this.props.history.push('/welcome')
      /** 
        // With error handling
        let body = {
          'user': name,
          'pass': password
        };
        fetch("http://127.0.0.1:5000/auth", {
          method: "POST",
          body: JSON.stringify(body),
          headers: {
            "Content-type": "application/json; charset=UTF-8"
          }
        })
        .then(response => response.json())
        .then(data => {
          console.log('Success:', data['name']);
          this.props.passName(data['name'])
          data['message'] === 200 ? this.props.history.push('/welcome') : this.setState(() => ({error: !this.state.error}))
        })
          .catch(error => {
            console.error(
              "There has been a problem with your fetch operation:",
              error
            );
          });
        */
    }

    handleClickSignUp = (e) => {
        e.preventDefault();
        const name = e.target.elements.user.value.trim();
        const password = e.target.elements.password.value.trim();
        this.props.passName("Calvin");
        this.props.history.push('/welcome');
        //this.getUsers(name, password);
    }

    render(){
        return (
            <div className="SignIn" style={{display: this.state.display }}>
                <form onSubmit={this.handleClickSignUp}>
                    <input type="text" placeholder="Username" name="user" required/>
                    <input type="password" placeholder="Password" name="password" required/>
                    <button>Sign in</button>
                </form>
                {this.state.error && <p>Incorrect credentials</p>}
            </div>
        );
    }
}

export default withRouter(SignIn);