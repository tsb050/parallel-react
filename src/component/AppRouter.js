import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import App from './App';
import Welcome from './Welcome';
import Explore from './Explore';

class AppRouter extends React.Component {

    state = {
        username: '',
        authed: false
    }

    handlePassFrom = (val) => {
        if(val){
            this.setState(() => ({username: val}))
            this.setState(() => ({authed: true}))
        }
    }

    render(){
        return (
            <BrowserRouter>
                <div className="one">
                    <Switch>
                        <Route path="/" render={(props) => (
                            <App {...props} sendFromApp={this.handlePassFrom} />
                        )} exact={true} />
                        <Route path="/welcome" render={(props) => (
                            <Welcome {...props} isAuthed={this.state.authed} myName={this.state.username} />
                        )} />
                        <Route path="/explore" render={(props) => (
                            <Explore {...props} myName={this.state.username} />
                        )} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
};

export default AppRouter;